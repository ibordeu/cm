/* Program for obtaining unknown y-axis values through normal cubic spline */
/*Inclusion of stdio.h for using printf*/
#include <stdio.h>
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <fstream>
#include <random>

int main()
{
    // we make a file for printing the random numbers
    FILE *RandSin;
    RandSin = fopen("RandSin.txt", "w");
    // we define the Mersenne twister as our PRNG with seed=2.1
    std::mt19937 generator (2);
    // we define the number distribution to be uniform in the range [0,1]
    std::uniform_real_distribution<double> dis(0.0, 1.0);
    
    // we define the vector that will contain the 10^5 random numbers
    for (int i=0; i<100000; i++) {
        // we use the Mersenne twister to generate 10E5 random numbers
        double randoms = dis(generator);
        // we operate random numbers with the desired distribution via the transformation method
        randoms = acos(1-2*randoms);
        // we print the random numbers to the file
        fprintf(RandSin, "%f\n",randoms);
    }
    fclose(RandSin);
    //return 0;
}
/*
 Ignacio Bordeu
 28/11/2015
*/

