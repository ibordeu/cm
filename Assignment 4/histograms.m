clear all; clc;

%% code loads file from uniform distribution and plots histogram %%%%%%%
x1 = load('RandUniform.txt');
n1 = calcnbins(x1,'fd'); % calculate bins with Freedman-Diaconis method.
[nelements1,bincenter1] = hist(x1,n1);
figure(1); bar(bincenter1,nelements1,'k');
xlabel('Bins');
ylabel('Number of samples');

%% code loads file for 0.5*sin(x) distribution, normalizes it and plots pdf
x2 = load('RandSin.txt');
x = 0:0.0001:pi;
fx1 = 0.5*sin(x);
n2 = calcnbins(x2,'fd'); % calculate bins with Freedman-Diaconis method. addd = sum(xx);
[nelements2,bincenter2] = hist(x2,n2);
s2 = sum(nelements2)*pi./n2; % calculate the normalization value
figure(2); bar(bincenter2,nelements2/s2,'k');
hold on
plot(x,fx1);
hold off
axis([0 pi 0 0.6])
xlabel('Bins');
ylabel('PDF');

%% loads file for (2/pi)*sin(x)^2 distribution and plots PDF
x3 = load('RandSin2.txt');
fx2 = (2/pi)*sin(x).^2;
n3 = calcnbins(x3,'fd'); % calculate bins with Freedman-Diaconis method.
[nelements3,bincenter3] = hist(x3,n3);
s3 = sum(nelements3)*pi./n3; % calculate the normalization value
figure(3);bar(bincenter3,nelements3./s3,'k');
hold on
plot(x,fx2);
hold off
axis([0 pi 0 0.7])
xlabel('Bins');
ylabel('PDF');

%% f(x) c(x) and cdf(x) are ploted for Q3
c = sin(x);
cdf = 1-cos(x);
figure(4); plot(x,fx2,x,c,x,cdf);
axis([0 pi 0 2])
xlabel('x');
ylabel('Distributions');
legend('y(x) = (2/pi)sin^2(x)','c(x) = sin(x)','cdf(x)=1-cos(x)');
