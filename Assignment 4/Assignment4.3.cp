/* Program for obtaining unknown y-axis values through normal cubic spline */
/*Inclusion of stdio.h for using printf*/
#include <stdio.h>
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <fstream>
#include <random>

int main()
{
    double pi = atan(1.0)*4.0;
    double x0,c,f;
    // we make a file for printing the random numbers
    FILE *RandSin2;
    RandSin2 = fopen("RandSin2.txt", "w");
    // we define the Mersenne twister as our PRNG with seed=2.1
    std::mt19937 generator (2);
    // we define the number distribution to be uniform in the range [0,1]
    std::uniform_real_distribution<double> dis(0.0, 1.0);
    
    // we define the vector that will contain the 10^5 random numbers
    for (int i=0; i<100000;) {
        // we use the Mersenne twister to generate 10E5 random numbers
        double random1 = 2*dis(generator), random2 = dis(generator);
        // we operate random numbers with the desired distribution via the transformation method
        x0 = acos(1-2*random1);
        c = sin(x0);
        random2 = random2*c;
        f = (2/pi)*pow(sin(x0),2);
        if (random2<=f) {
            fprintf(RandSin2, "%f\n",x0);
            i++;
        }
        // we print the random numbers to the file
    }
    fclose(RandSin2);
    //return 0;
}
/*
 Ignacio Bordeu
 28/11/2015
*/

